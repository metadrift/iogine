declare const DEBUG5: string
declare const DEBUG4: string
declare const DEBUG3: string
declare const DEBUG2: string
declare const DEBUG1: string
declare const LOG: string
declare const INFO: string
declare const NOTICE: string
declare const WARNING: string
declare const ERROR: string

interface ISqlRow {
  [key: string]: any
}

type PlV8Func = (...args: any[]) => any

interface ICursor {
  fetch: (nrows?: number) => ISqlRow[] | ISqlRow
  move: (nrows: number) => void
  close: () => void
}

interface IPreparedStatement {
  execute: (...args: string[]) => ISqlRow[] | number
  cursor: (sql: string, ...args: string[]) => ICursor
  free: () => void
}

interface IPlv8 {
  elog: (level: string, message: string) => void
  quote_literal: (quotable: string) => string
  quote_nullable: (quotable: string) => string
  quote_indent: (quotable: string) => string
  find_function: (functionName: string, ...types: any[]) => PlV8Func
  version: string
  execute: (sql: string, ...args: string[]) => ISqlRow[] | number
  prepare: (sql: string, ...args: string[]) => IPreparedStatement
  subtransaction: (exe: () => void) => void
}

declare const plv8: IPlv8
