
export PROJECT_ROOT := $(shell pwd)
DEV_COMPOSE := "${PROJECT_ROOT}/docker/dev.compose.yml"
SHFMT=shfmt -i 2 -ci

.PHONY: help check-formatting lint format build-ci-image validate \
        install-deps shell system-up system-down system-restart \
		deploy-ci-image-to-registry shell-lint check-node-formatting \
		node-lint check-shell-formatting node-format shell-format

.DEFAULT_GOAL := help

##########################################################################
# Commands
##########################################################################

help: ## Print this helptext
	@egrep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

##########################################################################
# Docker compose commands
##########################################################################

system-down: ## Teardown the development system, removing all containers
	docker-compose -f "${DEV_COMPOSE}" down --remove-orphans -v

system-restart: ## Restart all containers in the development system
	docker-compose -f "${DEV_COMPOSE}" restart

system-up: ## Bring up the development system
	docker-compose -f "${DEV_COMPOSE}" up -d

shell: system-up
	${RUN_IN_CONTAINER} "/bin/bash"

##########################################################################
# Docker commands
##########################################################################
build-ci-image: ## build the docker image used in the CI process
	docker build -t iogine-ci:latest -f ./docker/Dockerfile .

deploy-ci-image-to-registry: ## build the ci container and push it to the gitlab registry. must be logged in already
	${PROJECT_ROOT}/docker/src/deploy-ci-image-to-repository.sh

##########################################################################
# Checks
##########################################################################

check-shell-formatting: ## check the formatting of the shell scripts
	${SHFMT} -d docker/src

check-node-formatting: ## check the formating, error if things are not formatted
	npm run check-format

check-formatting: check-shell-formatting check-node-formatting ## Check all source formatting in the project

shell-lint: ## lint the shell scripts
	shellcheck docker/src/*.sh

node-lint: ## run linting on the node sources
	npm run lint

lint: shell-lint node-lint ## run the linter. error out if the system doesn't lint

validate: lint check-formatting ## Run all checks to validate that this is ready for merging

##########################################################################
# Modification commands
##########################################################################
install-deps: ## run npm install
	npm install

shell-format: ## run formatting on the shell scripts in the project
	${SHFMT} -w docker/src/

node-format: ## format all of the source in the system
	npm run format

format: node-format shell-format ## format all sources in the project