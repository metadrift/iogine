import * as commandLineArgs from 'command-line-args'
import * as commandLineUsage from 'command-line-usage'
import * as fs from 'fs'
import * as yaml from 'js-yaml'
import * as path from 'path'
import * as transform from './transform'
import * as types from './types'

const runServer = (file: string, outputFile?: string): void => {
  const schema = yaml.safeLoad(fs.readFileSync(file, 'utf8')) as types.ISchema
  const options: types.IOptions = { basePath: path.dirname(file) }
  const sqlData = transform.transform(options, schema)
  if (outputFile) {
    fs.writeFileSync(outputFile, sqlData, 'utf-8')
  } else {
    // tslint:disable-next-line
    console.log(sqlData)
  }
}

export const main = () => {
  const optionDefinitions = [
    {
      alias: 'h',
      description: 'Display this usage guide.',
      name: 'help',
      type: Boolean,
    },
    {
      alias: 'f',
      description: 'The driving yaml file.',
      name: 'file',
      type: String,
    },
    {
      alias: 'o',
      description: 'The file to write two',
      name: 'outfile',
      type: String,
    },
  ]

  const optionDescription = [
    {
      content: 'Generates a schema.sql for postgres',
      header: 'pl-v8-typescript',
    },
    {
      header: 'Options',
      optionList: optionDefinitions,
    },
  ]

  const desc = commandLineUsage(optionDescription)
  const options = commandLineArgs(optionDefinitions)

  const valid = options.file

  if (valid && !options.help) {
    runServer(options.file, options.outfile)
  } else {
    // tslint:disable-next-line
    console.log(desc)
  }
}
