import * as fs from 'fs'
import * as path from 'path'
import * as types from './types'

type MultiplexerFun = (sql: types.Sql) => string

// tslint:disable-next-line
export const typescriptFunHead = require('!!raw-loader!./plv8')

export const multiplexer = (options: types.IOptions): MultiplexerFun => (
  sql: types.Sql,
): string => {
  if (types.isSqlLiteral(sql)) {
    return `// ${sql.name}\n\n ${sql.sql}`
  } else if (types.isSqlFileReference(sql)) {
    const data = fs.readFileSync(path.join(options.basePath, sql.path), 'utf-8')
    return `// ${sql.name}\n\n${data}`
  } else {
    // @ts-ignore -- this exists so the typechecker will force exhaustiveif statements
    // tslint:disable-next-line
    const _exhaustiveCheck: never = sql
    throw new Error(
      ` ${JSON.stringify(
        sql,
      )}: This should never occur - execution arrived at an impossible path`,
    )
  }
}

export const transform = (
  options: types.IOptions,
  schema: types.ISchema,
): string => {
  return schema.sql.map(multiplexer(options)).join('\n')
}
