export interface ISqlLiteral {
  name: string
  sql: string
}

export interface ISqlFileReference {
  name: string
  path: string
}

export type Sql = ISqlLiteral | ISqlFileReference

export interface ISchema {
  definitions?: string[]
  sql: Sql[]
}

export interface IOptions {
  basePath: string
}

export const isSqlLiteral = (sql: Sql): sql is ISqlLiteral => {
  return (sql as ISqlLiteral).sql !== undefined
}

export const isSqlFileReference = (sql: Sql): sql is ISqlFileReference => {
  return (sql as ISqlFileReference).path !== undefined
}
