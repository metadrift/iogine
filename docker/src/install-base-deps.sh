#! /bin/bash

set -o errexit \
  -o nounset \
  -o xtrace \
  -o pipefail

FLYWAY_VERSION=6.0.0-beta

function install_migra() {
  install_clean python python-pip python-setuptools
  pip install migra
  migra --help
}

function install_flyway() {

  mkdir -p "$FLYWAY_DIR"

  install_clean openjdk-8-jdk

  cd "$FLYWAY_DIR" || exit 1
  curl -L "https://repo1.maven.org/maven2/org/flywaydb/flyway-commandline/${FLYWAY_VERSION}/flyway-commandline-${FLYWAY_VERSION}.tar.gz" \
    -o "flyway-commandline-${FLYWAY_VERSION}.tar.gz"

  tar -xzf "flyway-commandline-${FLYWAY_VERSION}.tar.gz" \
    --strip-components=1
  rm "flyway-commandline-${FLYWAY_VERSION}.tar.gz"
  ./flyway -v
}

function install_nodejs() {
  install_clean curl
  curl -sL https://deb.nodesource.com/setup_10.x | bash -
}

function main() {
  install_flyway
  install_migra
  install_nodejs
}

main "$@"
