#!/usr/bin/env bash

set -o errexit \
  -o nounset \
  -o xtrace \
  -o pipefail

function deploy_current_container() {
  local version="$1"
  local ci_registry="$2"

  local image_tag="iogine-ci:${version}"

  docker build -t "${image_tag}" -f ./docker/Dockerfile .
  docker tag "${image_tag}" "${ci_registry}/metadrift/iogine/iogine-ci:latest"
  docker tag "${image_tag}" "${ci_registry}/metadrift/iogine/iogine-ci:${version}"
  docker push "${ci_registry}/metadrift/iogine/iogine-ci"
}

function main() {

  local current_dir
  local version
  local ci_registry

  current_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"
  version=$("${current_dir}/git-version.sh")
  ci_registry=${CI_REGISTRY:-}

  if [[ -z "${ci_registry}" ]]; then
    ci_registry="registry.gitlab.com"
    echo "No \$CI_REGISTRY environment variable set, using ${ci_registry}"
  fi

  deploy_current_container "${version}" "${ci_registry}"
}

main
