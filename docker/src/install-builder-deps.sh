#! /bin/bash

set -o errexit \
  -o nounset \
  -o xtrace \
  -o pipefail

function install_go() {
  install_clean golang
}

function install_shfmt() {
  go get -u mvdan.cc/sh/cmd/shfmt
  mv ~/go/bin/shfmt /usr/local/bin
}

function remove_go() {
  apt-get remove -y golang
}

function install_go_tools() {
  install_go
  install_shfmt
  remove_go
}

function install_migra() {
  pip install migra
}

function install_sql_format() {
  pip install sqlparse
}

function install_shellcheck() {
  install_clean shellcheck
}

function install_python_tools() {
  install_clean python-pip
  install_sql_format
  install_migra
}

function main() {
  install_go_tools
  install_python_tools
  install_shellcheck
}

main "$@"
