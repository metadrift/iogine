=

This is an opinionated project layout system for Postgresql that handles
database schema layout and migration. It requires that the target Postgresql
instance have the PLV8 language extension in place.

The target output of this compiler is a set of [FlywayDb](https://flywaydb.org)
migration files that you can use to upgrade and manage your system. The
expectation is that these files will be added to source control and deployed
normally.
